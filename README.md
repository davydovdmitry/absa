# ABSA
This is pipeline for aspect-based sentiment analysis of texts.
It's essential stages:

![](notebooks/images/pipeline.svg)

# Setup

**1) Requirements**

- [Python](https://www.python.org/downloads/) >= 3.7
- [Poetry](https://python-poetry.org/docs/) >= 0.12
- [PyTorch](https://pytorch.org/get-started/locally/) >= 1.5

----------
Execution
----------
```shell script
python train.py    # to train classifiers
python example.py  # to process text in example directory
```
